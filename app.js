const express = require('express');
const Router = require('./src/Misc/Router');
const bodyParser = require('body-parser');
const Jimple = require('jimple');
const Configture = require('configture');
const LoggerBuilder = require('./src/Misc/LoggerBuilder');
const { Server } = require('http');
const amqp = require('amqplib-easy');
const redis = require('redis');

const config = new Configture({}).load();
const app = express();
const logger = LoggerBuilder.build(config.logger);
const container = new Jimple();

const server = Server(app);
const io = require('socket.io')(server);
const redisAdapter = require('socket.io-redis');

io.adapter(redisAdapter(config.redis));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

const {
  user, password, host, port
} = config.amqp;

container.set('app', app);
container.set('config', config);
container.set('logger', logger);
container.set('io', io);
container.set('amqp', amqp(`amqp://${user}:${password}@${host}:${port}/`));
container.set('redis', redis.createClient(config.redis));

const router = new Router(container);
router.load();

module.exports = {
  app, container, io, server
};
