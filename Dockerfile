FROM node:9.11.1

LABEL maintainer="Michael INTHILITH <minthilith@gmail.com>"

ENV APP_NAME sr-test
ENV APP_PATH /opt/${APP_NAME}
ENV HOSTNAME sr-test
ENV NODE_NO_WARNINGS 1

RUN rm -rf /var/lib/apt/lists/*

RUN mkdir ${APP_PATH}
VOLUME ${APP_PATH}
WORKDIR ${APP_PATH}

USER node

EXPOSE 3000
EXPOSE 5959

RUN npm install

CMD ["npm", "start"]
