# SR

## Prerequisite

* [docker](https://www.docker.com/community-edition#/download)
* [docker-compose](https://docs.docker.com/compose/install/)

## Getting started
You launch the compose file in your prefered shell, typing:
```bash
docker-compose build && docker-compose up -d

```

## Have a check
* [api1](http://localhost:3000)
* [api2](http://localhost:3001)
* [portainer](http://localhost:9000)
* [rabbitmq-management](http://localhost:15672) (user/pass: rabbitmq/rabbitmq)

## Questions
* Which share of web users will this implementation address? Should it be increased and
how?

This implementation can be use by every browser that support native javascript. 

* How many users can connect to one server?

It depends on the average size of the payload we have to handle and the type of the server we are using, but my guess would be that socketIO would be the bottleneck.
For this implementation, I didn't manage to make server goes for a crash before my headless chrome dies.

* How can the system support more systems?

I used a redis adapter that allows to clusterize socketIO.
In order to support more traffic, we should use a redis sentinel or an even more efficient redis cluster.   

* How to reduce the attack surface of the systems?

I did not use socketIO client to emit message directly, I put some queuing system that bufferizes incoming message first.
That allows us to be able to absorb a lot more incoming messages.

* Should an authentication mechanism be put in place and if yes, how?

We could think about a simple authentication mecanism, such as JWT or open oauth (such as github/gmail...).
Having to authenticate, and check if user are know, will undeniably be costly