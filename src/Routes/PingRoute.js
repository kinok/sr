const BaseRoute = require('./BaseRoute');
const { version } = require('../../package.json');
const { hostname } = require('os');

class PingRoute extends BaseRoute {
  get config() {
    return {
      path: '/ping'
    };
  }

  get() {
    return (req, res) => {
      res.json({
        ping: true,
        version,
        hostname: hostname(),
        date: new Date(),
      });
    };
  }
}

module.exports = PingRoute;
