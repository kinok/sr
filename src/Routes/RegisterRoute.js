const BaseRoute = require('./BaseRoute');

class RegisterRoute extends BaseRoute {
  get config() {
    return {
      path: '/register'
    };
  }

  post() {
    return async (req, res) => {
      const { id, name } = req.body;
      await this._container.get('redis').hmset('users', [id, name]);
      res.status(204).send();
    };
  }
}

module.exports = RegisterRoute;
