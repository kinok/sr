const _ = require('lodash');

class BaseRoute {
  constructor(container) {
    this._container = container;
  }

  static get methods() {
    return [
      'get',
      'post',
      'put',
      'delete',
      'head',
    ];
  }

  static get defaultConfig() {
    return {
      path: '/',
      middleware: [],
    };
  }

  get routeConfig() {
    return _.assign(BaseRoute.defaultConfig, this.config);
  }

  get config() {
    return {};
  }

  register() {
    if (_.has(this.routeConfig, 'middleware') && this.routeConfig.middleware.length) {
      _.forEach(this.config.middleware, middlewareName => {
        // eslint-disable-next-line global-require, import/no-dynamic-require
        const Middleware = require(`../Middlewares/${middlewareName}`);
        const middleware = new Middleware(this._container);
        this._container.get('app').route(this.routeConfig.path).all(middleware.middleware);
      });
    }

    _.forEach(BaseRoute.methods, method => {
      this._container.get('app')[method](this.routeConfig.path, this[method]());
    });
  }

  notImplemented(req, res) {
    return res.status(404).json({
      code: 404,
      err: 'not found'
    });
  }

  get() {
    return this.notImplemented;
  }

  post() {
    return this.notImplemented;
  }

  put() {
    return this.notImplemented;
  }

  delete() {
    return this.notImplemented;
  }

  head() {
    return this.notImplemented;
  }
}

module.exports = BaseRoute;
