const BaseRoute = require('./BaseRoute');

class MessageRoute extends BaseRoute {
  get config() {
    return {
      path: '/message'
    };
  }

  post() {
    return async (req, res) => {
      try {
        const { message, from, to } = req.body;

        this._container.get('amqp').publish({
          exchange: 'chat'
        }, 'chat.*', {
          ts: Date.now(),
          message,
          from: from || 'unknown',
          to: to || 'all',
        });
        return res.status(204).send();
      } catch (err) {
        return res.status(500).json({
          err
        });
      }
    };
  }
}

module.exports = MessageRoute;
