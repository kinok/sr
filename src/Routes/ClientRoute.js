const BaseRoute = require('./BaseRoute');

class ClientRoute extends BaseRoute {
  get config() {
    return {
      path: '/client'
    };
  }

  get() {
    return (req, res) => {
      this._container.get('redis').hgetall('users', (err, clients) => {
        if (err) {
          return res.status(500).json({
            err: err.message
          });
        }
        return res.status(200).json(clients);
      });
    };
  }
}

module.exports = ClientRoute;
