const winston = require('winston');
const _ = require('lodash');

class LoggerBuilder {
  static build(config = {}) {
    return new (winston.Logger)({
      transports: [
        new (winston.transports.Console)(_.defaults(config, {
          level: 'info'
        }))
      ]
    });
  }
}

module.exports = LoggerBuilder;
