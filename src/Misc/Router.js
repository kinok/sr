const fs = require('fs');
const path = require('path');
const _ = require('lodash');

class Router {
  constructor(container) {
    this._container = container;
  }

  load(directory = './src/Routes') {
    const fileList = this.walk(directory);
    _.forEach(fileList, file => {
      this._container.get('logger').debug('Loading route %s', file);
      // eslint-disable-next-line import/no-dynamic-require, global-require
      const Route = require(file);
      const route = new Route(this._container);
      route.register();
    });
  }

  walk(dir, fileList) {
    const files = fs.readdirSync(dir);
    let fl = fileList || [];
    _.forEach(files, (file) => {
      const fullpath = path.join(dir, file);
      if (fs.statSync(fullpath).isDirectory()) {
        fl = this.walk(fullpath, fl);
      } else {
        fl.push(fs.realpathSync(fullpath));
      }
    });
    return fl;
  }
}

module.exports = Router;
